// ignore_for_file: unused_import

import 'dart:developer';
import 'dart:io';

import 'package:dart_monkey/dart_monkey.dart';
import 'package:dart_monkey_example/controller.dart';

import 'main.reflectable.dart';

Future<void> initializeLocalDatabase(MonkeyServer server) async {
  await server.connectToDatabase(
    conn: DatabaseConnection.mysql,
    settings: DatabaseSettings(
      username: "root",
      password: "secret",
      database: "monkey_test_database",
    ),
  );
}

Future<void> initializeDockerDatabase(MonkeyServer server) async {
  // Our docker environment takes time to startup mysql. Thus give
  // multiple tries to connect, with delays in between.
  const int maxTries = 10;
  for (int i = 0; i < maxTries; i++) {
    try {
      await server.connectToDatabase(
        conn: DatabaseConnection.mysql,
        settings: DatabaseSettings(
          username: "root",
          password: "secret",
          database: "monkey_example_database",
          host: 'db',
        ),
      );
      log("Database connection established");
      break;
    } catch (e) {
      if (i == maxTries - 1) {
        rethrow;
      }
      log("Trying to establish database connection..");
      sleep(const Duration(seconds: 5));
    }
  }
}

void main() async {
  final server = MonkeyServer();
  log("Dart Monkey ${server.version()}");

  // Initialize reflection for our model
  initializeReflectable();

  // Initialize database
  initializeDockerDatabase(server);
  //initializeLocalDatabase(server);

  // Route to our controller
  final controller = SampleController();
  server.route("/v1/resources", controller);

  // Start server
  await server.start(address: InternetAddress.anyIPv4, port: 8080);
}
