
# Sample for Dart Monkey

This is a sample project for the Dart Monkey server framework:
https://gitlab.com/ZottelDev/dart_monkey/

## Structure

- bin/main.dart: Initializes the server, including database and routes
- lib/controller.dart: Controller for the 'resources' endpoint in this sample
- lib/model.dart: Model used by the controller

## How to run

### Locally

- Execute 'dart run build_runner build' in the terminal, to auto generate the reflection files (via 'Reflectable')
- Make sure you have an SQL server running locally
- Change main.dart to call initializeLocalDatabase(). Adjust the DB configuration as required
- Start the server from within Visual Studio Code. With full debugging support
- The endpoint is available via http://localhost:8080/v1/resources

### Docker (for deployment)

- initializeDockerDatabase
- Change main.dart to call initializeDockerDatabase()
- Execute 'docker-compose up' in the root folder to build and run the docker image. The image includes both the server and a MySQL service. It is configured to execute SQL scripts in the 'initdb' folder on startup, to create the database and table.