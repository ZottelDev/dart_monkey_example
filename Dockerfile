# ==== STEP 1: Build Dart server application ====

FROM dart:stable AS build

WORKDIR /app

# Resolve app dependencies.
COPY pubspec.* ./
RUN dart pub get

# Copy app source code and compile it.
COPY . .
RUN dart pub get --offline
RUN dart run build_runner build
RUN dart compile exe bin/main.dart -o server


# ==== STEP 2: Create image with minimal container and automatic startup ====

# Build minimal serving image from compiled `/app/server` and required system
# libraries and configuration files stored in `/runtime/` from the build stage.
FROM scratch
COPY --from=build /runtime/ /
COPY --from=build /app/server /app/monkey-server/

# Start server.
CMD ["/app/monkey-server/server"]