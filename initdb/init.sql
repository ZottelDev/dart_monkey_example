DROP DATABASE IF EXISTS monkey_example_database;
CREATE DATABASE monkey_example_database;

USE monkey_example_database;

CREATE TABLE IF NOT EXISTS sample_resources (
      id INTEGER NOT NULL AUTO_INCREMENT,
      name VARCHAR(255) NOT NULL,
      created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (id)
    );