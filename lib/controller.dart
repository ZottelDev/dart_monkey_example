import 'package:dart_monkey/dart_monkey.dart';
import 'package:dart_monkey_example/model.dart';

class SampleController extends Controller {
  @override
  Future<ApiResponse> create(
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) async {
    // Validate input
    validate(validators: [StringValidator(path: 'name', required: true)]);

    // Store the new client
    final resource = SampleResource();
    resource.name = attributes['name'] as String;
    await resource.save();

    // Return the response
    return ApiResponse.resource(
      statusCode: 201,
      resource: resource,
      request: request,
    );
  }

  @override
  Future<ApiResponse> delete(
    String id,
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) async {
    return ApiResponse.error(statusCode: 400, title: "delete for id $id");
  }

  @override
  Future<ApiResponse> read(
    String id,
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) async {
    final resource = await Search.whereOrFail<SampleResource>('id', id);
    return ApiResponse.resource(
      statusCode: 200,
      resource: resource,
      request: request,
    );
  }

  @override
  Future<ApiResponse> readAll(
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) async {
    return ApiResponse.error(statusCode: 400, title: "readAll");
  }

  @override
  Future<ApiResponse> update(
    String id,
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) async {
    return ApiResponse.error(statusCode: 400, title: "update for id $id");
  }
}
